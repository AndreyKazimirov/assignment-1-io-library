%define SYS_EXIT 60
%define SYS_WRITE 1
%define STDOUT 1
%define TAB 0x9
%define SPACE 0x20
%define LINE 0xA

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax         
    .counter:
        cmp byte [rdi + rax], 0   
        je .return       
        inc rax          
        jmp .counter       
    .return:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi                
    call string_length ; получаем длину строки в rax     
    pop rdi                 
    mov rdx, rax       ; устонавливаем  rdx  на длину строки     
    mov rsi, rdi       ; rsi начало строки     
    mov rdi, STDOUT             
    mov rax, SYS_WRITE              
    syscall                 
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi             
    mov rsi, rsp         
    mov rax, SYS_WRITE           
    mov rdx, 1           
    mov rdi, STDOUT          
    syscall              
    pop rdi              
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, LINE         
    jmp print_char       

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    push rbx
    mov r8, rsp
    mov rbx, 10
    push 0
    .cicle:
        xor rdx, rdx
        div rbx
        add rdx, '0'        ;преобразование остатка в ASCII
        dec rsp
        mov byte[rsp], dl   ; сохранение цифр в Стек
        test rax, rax
        je .finish
        jmp .cicle
    .finish:
        mov rdi, rsp
        lea rsp, [r8 - 32]
        push r8
        call print_string
        pop r8
        mov rsp, r8
        pop rbx
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0           
    jge .plus       
    neg rdi              
    push rdi             
    mov rdi, '-'         
    call print_char      
    pop rdi              
    .plus:
		jmp print_uint   

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .cicle:
        mov r11b, byte [rsi]       
        cmp byte [rdi], r11b       
        jne .neq                   
        test r11b, r11b            
        jz .eq                     
        inc rsi                    
        inc rdi                    
        jmp .cicle                  
    .neq:
        mov rax, 0            ; когда не равны     
        ret
    .eq:
        mov rax, 1            ; когда равны     
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0             
    mov rdi, 0             
    push 0                 
    mov rsi, rsp           
    mov rdx, 1             
    syscall                
    pop rax                
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx             
    .cicle:
        cmp rcx, rsi         
        jge .fail            
        push rdi             
        push rsi             
        push rcx             
        call read_char       
        pop rcx              
        pop rsi              
        pop rdi              
        cmp al, SPACE   ;сравнение символа с пробелом        
        je .space       
        cmp al, TAB     ; сравнение символа с табуляцией       
        je .space       
        cmp al, LINE    ; сравнение символа в новой строкой     
        je .space       
        mov [rdi + rcx], al  
        cmp al, 0            
        je .fine             
        inc rcx              
        jmp .cicle            
    .fine:
        mov rax, rdi         
        mov rdx, rcx         
        ret
    .fail:
        xor rax, rax         
        ret
    .space:
        test rcx, rcx        
        jz .cicle             
        jmp .fine

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    xor rdx, rdx             
    xor rax, rax             
    xor rbx, rbx             
    .cicle:
        mov bl, byte [rdi + rdx]  
        sub bl, '0'               
        jl .return                
        cmp bl, 9
        jg .return                
        push rdx                  
        mov rdx, 10               
        mul rdx                   
        pop rdx                   
        add rax, rbx              
        inc rdx                   
        jmp .cicle                 
    .return:
        pop rbx                   
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'        
    je .minus               
    jmp parse_uint             
	.minus:
		inc rdi                
		call parse_uint        
		cmp rdx, 0             
		je .fail               
		inc rdx                
		neg rax                
		ret
	.fail:
		mov rax, 0             
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax        
    .cycle:
        mov r10b, byte [rdi + rax]  ;загрузка из исходной строки текущего байта
        mov byte [rsi + rax], r10b  ;сохранение байта в цулевой строке
        inc rax
        cmp rdx, rax    
        js .error
        test r10b, r10b             ; проверка на нулевой байт 
        jne .cycle
        ret
    .error:
        xor rax, rax                ; очитска rax(везвращает 0 для указания на ошибку)
        ret           
                
    
